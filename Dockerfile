FROM pspdev/pspdev:latest

ENV DEBIAN_FRONTEND="noninteractive"

ARG uid
ARG branch=develop
ENV branch=$branch

RUN apk add \
    gmp \
    mpc1 \
    mpfr \
    sudo \
    make \
    git && \
    adduser -D -g '' -h /developer developer && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown -R developer:developer /developer

ENV HOME=/developer

USER developer
WORKDIR /developer
VOLUME /developer

CMD /bin/bash
